My own ubuntu environment for daily use.

Curently supported packages:

- build-essential
- git
- openjdk-8-jdk and gradle default version support
- cmake for C++ development
- GRPC
- Protobuf
- Latex
