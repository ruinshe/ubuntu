# My own ubuntu environment for daily use
#

FROM ubuntu:bionic

RUN apt-get update

RUN apt-get install -y apt-utils
RUN apt-get install -y build-essential openjdk-8-jdk gradle cmake git libssl-dev

# GRPC configuration
RUN git clone -b v1.17.1 https://github.com/grpc/grpc.git --depth=1 /tmp/grpc
WORKDIR /tmp/grpc
RUN git submodule update --init --recursive third_party/cares/cares
RUN git submodule update --init --recursive third_party/zlib
RUN git submodule update --init --recursive third_party/protobuf

WORKDIR /tmp/grpc/third_party/cares/cares/build
RUN cmake -DCMAKE_BUILD_TYPE=Release ..
RUN make -j4 install

WORKDIR /tmp/grpc/third_party/zlib/build
RUN cmake -DCMAKE_BUILD_TYPE=Release ..
RUN make -j4 install

WORKDIR /tmp/grpc/third_party/protobuf/build
RUN cmake -Dprotobuf_BUILD_TESTS=OFF -DCMAKE_BUILD_TYPE=Release ../cmake
RUN make -j4 install

# Other modules
WORKDIR /tmp/grpc
RUN git submodule update --init --recursive third_party/gflags
RUN git submodule update --init --recursive third_party/benchmark

WORKDIR /tmp/grpc/build
RUN cmake -DgRPC_INSTALL=ON -DgRPC_BUILD_TESTS=OFF \
    -DgRPC_PROTOBUF_PROVIDER=package -DgRPC_ZLIB_PROVIDER=package  -DgRPC_CARES_PROVIDER=package \
    -DgRPC_SSL_PROVIDER=package -DCMAKE_BUILD_TYPE=Release ..
RUN make -j4 install

# Locales setting
RUN apt-get install -y locales
RUN locale-gen en_US.UTF-8 zh_CN.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Latex
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get install -y texlive-full
RUN apt-get install -y latex-cjk-chinese

# Java settings.
ENV JAVA_HOME /usr/lib/jvm/java-1.8.0-openjdk-amd64
RUN update-java-alternatives --set "${JAVA_HOME}" && java -version && javac -version

# Network utils
RUN apt-get install -y dnsutils inetutils-ping

RUN rm -rf /tmp/*

CMD ["/bin/bash"]
